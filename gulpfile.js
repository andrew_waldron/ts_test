var gulp = require('gulp');
var rename = require("gulp-rename");
var ts = require('gulp-typescript');
var tsd = require('gulp-tsd');
//var tsProject = plugins.typescript.createProject('src/tsconfig.json');

gulp.task('default', ['setup-js-deps', 'compile-ts']);

gulp.task('setup-js-deps', function() {
  return gulp.src('bower_components/jquery/dist/jquery.min.js')
    .pipe(gulp.dest('dist/src'));
});

gulp.task('compile-ts', ['install-ts-deps'], function() {
  return gulp.src('src/**/*.ts')
    .pipe(ts({
      noImplicitAny: true,
      out: 'app.js'
    }))
    .pipe(gulp.dest('dist/src'));
});

gulp.task('install-ts-deps', function() {
  tsd({
    command: 'reinstall',
    config: './tsd.json'
  });
});
